# iniConfig

#### 介绍
Go基于ini文件的配置库



#### 使用案例

./config.ini
```
[server]
ip=10.2.16.9
port=8080

[mysql]
username=root
password=root
database=configtest
host=192.168.1.89
port=3306
timeout=1.111
```


```
package main

import (
	"fmt"
	"gitee.com/golang-samples/iniConfig"
	"io/ioutil"
)

type Config struct {
	ServerConf ServerConfig `ini:"server"`
	MysqlConf  MysqlConfig  `ini:"mysql"`
}

type ServerConfig struct {
	Ip   string `ini:"ip"`
	Port int    `ini:"port"`
}

type MysqlConfig struct {
	Username string  `ini:"username"`
	Password string  `ini:"password"`
	Database string  `ini:"database"`
	Host     string  `ini:"host"`
	Port     int     `ini:"port"`
	Timeout  float64 `ini:"timeout"`
}

func main() {
	fmt.Println("test ini config")
	data, _ := ioutil.ReadFile("./config.ini")
	var config Config
	iniconfig.UnMarshal(data, &config)
	fmt.Println(config)
}

```

package iniconfig

import (
	"fmt"
	"io/ioutil"
	"testing"
)

type Config struct {
	ServerConf ServerConfig `ini:"server"`
	MysqlConf  MysqlConfig  `ini:"mysql"`
}

type ServerConfig struct {
	Ip   string `ini:"ip"`
	Port int    `ini:"port"`
}

type MysqlConfig struct {
	Username string  `ini:"username"`
	Password string  `ini:"password"`
	Database string  `ini:"database"`
	Host     string  `ini:"host"`
	Port     int     `ini:"port"`
	Timeout  float64 `ini:"timeout"`
}

func TestIniConfigUnMarshal(t *testing.T) {
	fmt.Println(t.Name(), " begin...")
	data, err := ioutil.ReadFile("./config.ini")
	if err != nil {
		t.Error("open file error")
	}
	var config Config
	err = UnMarshal(data, &config)
	if err != nil {
		t.Errorf("file UnMarshal error:%v\n", err)
		return
	}
	t.Logf("UnMarshal success. config:%#v\n", config)
}

func TestIniConfigMarshal(t *testing.T) {
	fmt.Println(t.Name(), " begin...")
	serverConf := ServerConfig{
		Ip:   "10.1.2.1",
		Port: 9999,
	}
	mysqlConf := MysqlConfig{
		Username: "admin",
		Password: "admin123",
		Database: "db_admin",
		Host:     "192.168.1.2",
		Port:     3306,
		Timeout:  3.1415926,
	}
	config := Config{
		ServerConf: serverConf,
		MysqlConf:  mysqlConf,
	}
	resultData, err := Marshal(config)
	if err != nil {
		t.Errorf("file Marshal error:%v\n", err)
		return
	}
	err = ioutil.WriteFile("./config_marshal.ini", resultData, 0666)
	if err != nil {
		t.Errorf("file Marshal openfile error:%v\n", err)
		return
	}
	t.Logf("Marshal success. config_marshal.ini:\n%s \n", string(resultData))
}
